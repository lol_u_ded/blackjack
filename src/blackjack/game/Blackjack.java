package blackjack.game;

import blackjack.player.Dealer;
import blackjack.player.HumanPlayer;
import blackjack.player.Player;
import inout.In;
import inout.Out;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Blackjack {

    public static final int TOTAL_NUM_CARDS = 52;
    public static final int BLACKJACK = 21;

    private final HumanPlayer human;
    private final Dealer dealer;
    ArrayList<Card> deck;

    public Blackjack() {
        this.human = new HumanPlayer();
        this.dealer = new Dealer();
        this.deck = createDeck();
    }

    private ArrayList<Card> createDeck() {
        deck = new ArrayList<Card>();
        for (int i = 0; i <= TOTAL_NUM_CARDS - 1; i++) {
            Card card = new Card(i);
            deck.add(new Card(card.getIndex()));
        }
        Collections.shuffle(deck);
        return deck;
    }

    public Player getHumanPlayer() {
        return human;
    }

    public Player getDealer() {
        return dealer;
    }

    /**
     * evaluate which player won this round
     *
     * @return the result of this round
     */
    public GameResult evaluateCards() {
        int humanResult = getHumanPlayer().getValue();
        int dealerResult = getDealer().getValue();

        // Human got Blackjack
        if (getHumanPlayer().hasBlackJack()) {
            // both got a Blackjack :(
            if (getDealer().hasBlackJack()) {
                return GameResult.Draw;
            } else {
                stake = 3 * stake;
                return GameResult.PlayerWins;
            }
        }
        if (humanResult <= BLACKJACK) {

            if (humanResult == dealerResult) {
                return GameResult.Draw;
            }
            // Dealer is above Blackjack or lower human card value
            if (dealerResult > BLACKJACK || dealerResult < humanResult) {
                stake = 2 * stake;
                return GameResult.PlayerWins;
                // Dealer Card value is higher than human card value
            } else {
                return GameResult.DealerWins;
            }
        }
        // both card values are above Blackjack
        if (dealerResult > BLACKJACK) {
            return GameResult.Draw;
        }
        // Just human card value is above Blackjack
        return GameResult.DealerWins;
    }

    /**
     * draw a random card that is still in the deck
     *
     * @return the selected card
     * @throws OutOfCardsException when no more cards are available
     */
    public Card drawCard() {
        if (deck.isEmpty()) {
            throw new OutOfCardsException();
        }
        Card drawn = deck.get(0);
        deck.remove(0);
        return drawn;
    }

    private int stake = 0;

    /**
     * play the game
     */
    public void play() {
        boolean anotherRound = true;

        while (anotherRound) {

            boolean running = true;
            human.removeCoins(1);
            stake += 1;
            human.addCard(drawCard());
            human.addCard(drawCard());
            dealer.addCard(drawCard());
            printGameState();

            // Human turn
            while (running) {
                Turn humanTurn = human.makeTurn();
                if (humanTurn == Turn.DoubleDown) {
                    human.removeCoins(1);
                    stake += 1;
                    human.addCard(drawCard());
                    running = false;
                } else if (humanTurn == Turn.Stay) {
                    running = false;
                } else if (humanTurn == Turn.Hit) {
                    human.addCard(drawCard());
                    printGameState();
                }
            }

            // Dealer turn
            running = true;
            while (running) {
                Turn dealerTurn = dealer.makeTurn();
                if (dealerTurn == Turn.Stay) {
                    running = false;
                } else if (dealerTurn == Turn.Hit) {
                    dealer.addCard(drawCard());
                }
            }
            printGameState();
            GameResult gameResult = evaluateCards();
            if (gameResult == GameResult.Draw) {
                System.out.println("It`s a draw you will get back your stake");
                human.addCoins(stake);
                stake = 0;
                System.out.println("Your current Coins are now: " + human.getCoins());
            } else if (gameResult == GameResult.PlayerWins) {
                System.out.println("You won! You will receive " + stake + "coins ! :) ");
                human.addCoins(stake);
                stake = 0;
                System.out.println("Your current Coins are now: " + human.getCoins());
            } else if (gameResult == GameResult.DealerWins) {
                System.out.println("Against all odds you lost ...\n You will lose  " + stake + "coins.");
                stake = 0;
                System.out.println("Your current Coins are now: " + human.getCoins());
            }
            System.out.println("Next Round? (y/n)");
            char input = In.readChar();
            if (input == 'y' || input == 'Y') {
                System.out.println("\n\nStarting next round. Your coins: " + human.getCoins());
                dealer.nextRound();
                human.nextRound();
                deck = createDeck();
                System.out.println("Deck shuffled, cards removed. New game begins...");
            } else if (input == 'n' || input == 'N') {
                System.out.println("Okay, your final coin balance is:" + human.getCoins());
                anotherRound = false;
            }
        }
    }

    /**
     * print the cards of both players and their value
     */
    public void printGameState() {

        Out.println("Dealer (" + dealer.getValue() + ")");
        for (Card c : dealer.getCards()) {
            Out.print(c + " ");
        }
        Out.println();

        Out.println("Player (" + human.getValue() + ")");
        for (Card c : human.getCards()) {
            Out.print(c + " ");
        }
        Out.println("\n");
    }

}
