package blackjack.player;

import java.util.ArrayList;
import java.util.List;

import blackjack.game.Blackjack;
import blackjack.game.Card;
import blackjack.game.Turn;

public abstract class Player {

    private List<Card> cardsHold = new ArrayList<Card>();
    private int value = 0;

    public List<Card> getCardsHold() {
        return cardsHold;
    }

    public void nextRound() {
        cardsHold = new ArrayList<Card>();
        value = 0;
    }

    public void addCard(Card drawn) {
        cardsHold.add(drawn);
        updateValue();
    }

    private void updateValue() {
        int aces = 0;
        int currValue = 0;
        value = 0;

        for (Card card : cardsHold) {
            int index = card.getIndex();
            if (index == 1) {
                currValue += 11;
                aces++;
            } else currValue += Math.min(index, 10);
        }
        // add Aces
        while (aces > 0) {
            if (currValue > 21) {
                currValue -= 10;
            }
            aces--;
        }
        value = currValue;
    }

    // get value for all cards but aces
    private int getCardValue(int index) {
        if (index >= 11) {
            return 10;
        } else {
            return index;
        }
    }

    public List<Card> getCards() {
        return cardsHold;
    }

    public int getValue() {
        return value;
    }

    public boolean hasBlackJack() {
        return getValue() == Blackjack.BLACKJACK;
    }

    public abstract Turn makeTurn();
}
