package blackjack.player;

import blackjack.game.Turn;
import inout.In;

public class HumanPlayer extends Player {

    private int coins;

    public HumanPlayer() {
        this.coins = 10;
    }

    public int getCoins() {
        return coins;
    }

    public void addCoins(int get) {
        coins = coins + get;
    }

    public void removeCoins(int pay) {
        coins = coins - pay;
    }

    @Override
    public Turn makeTurn() {

        if (getValue() >= 21) {
            return Turn.Stay;
        }
        boolean dd = getCardsHold().size() == 2;
        if (dd) {
            System.out.println("Double Down ? --> press 'd'");
        }
        System.out.println("Hit ? --> press 'h'\nStay ? --> press 's'\n");
        char input = In.readChar();
        if (input == 'h') {
            return Turn.Hit;
        } else if (input == 'd' && dd) {
            return Turn.DoubleDown;
        } else if (input == 's') {
            return Turn.Stay;
        }
        return Turn.Stay;
    }

}
