package test;


import blackjack.game.Card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import blackjack.player.HumanPlayer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class PlayerTest {

    HumanPlayer humanPlayer;

    @BeforeEach
    public void setUpTest() {
        this.humanPlayer = new HumanPlayer();
    }

    @Test
    public void twoAces() {
        Card ace = new Card(Card.Color.Clubs, 1);
        Card ace2 = new Card(Card.Color.Diamonds, 1);
        humanPlayer.addCard(ace);
        assertEquals(11, humanPlayer.getValue());
        humanPlayer.addCard(ace2);
        assertEquals(12, humanPlayer.getValue());
    }

    @Test
    public void hasBlackJack() {
        Card ace = new Card(Card.Color.Clubs, 1);
        Card ten = new Card(Card.Color.Diamonds, 10);
        humanPlayer.addCard(ace);
        humanPlayer.addCard(ten);
        assertEquals(21, humanPlayer.getValue());
        assertTrue(humanPlayer.hasBlackJack());
    }

    @Test
    public void getValue() {
        Card three = new Card(Card.Color.Clubs, 3);
        Card two = new Card(Card.Color.Diamonds, 2);
        Card king = new Card(Card.Color.Diamonds, 13);
        humanPlayer.addCard(two);
        humanPlayer.addCard(three);
        humanPlayer.addCard(king);
        assertEquals(15, humanPlayer.getValue());
        assertFalse(humanPlayer.hasBlackJack());
    }

    @Test
    public void exactlyBlackJack() {
        Card king = new Card(Card.Color.Clubs, 13);
        Card queen = new Card(Card.Color.Diamonds, 12);
        Card ace = new Card(Card.Color.Diamonds, 1);
        humanPlayer.addCard(ace);
        humanPlayer.addCard(queen);
        humanPlayer.addCard(king);
        assertEquals(21, humanPlayer.getValue());
        assertTrue(humanPlayer.hasBlackJack());
    }

    @Test
    public void allAces() {
        Card ace = new Card(Card.Color.Clubs, 1);
        Card ace2 = new Card(Card.Color.Diamonds, 1);
        Card ace3 = new Card(Card.Color.Hearts, 1);
        Card ace4 = new Card(Card.Color.Spades, 1);
        humanPlayer.addCard(ace);
        assertEquals(11, humanPlayer.getValue());
        humanPlayer.addCard(ace2);
        assertEquals(12, humanPlayer.getValue());
        humanPlayer.addCard(ace3);
        assertEquals(13, humanPlayer.getValue());
        humanPlayer.addCard(ace4);
        assertEquals(14, humanPlayer.getValue());
    }

}