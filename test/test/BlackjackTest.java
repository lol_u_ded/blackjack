package test;

import blackjack.game.Blackjack;
import blackjack.game.Card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import blackjack.game.GameResult;
import blackjack.player.HumanPlayer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BlackjackTest {

    HumanPlayer humanPlayer;
    Blackjack testGame;

    @BeforeEach
    public void setUpTest() {

       this.humanPlayer = new HumanPlayer();
        this.testGame = new Blackjack();
    }

    @Test
    public void getValue() {
        Card three = new Card(Card.Color.Clubs, 3);
        Card two = new Card(Card.Color.Diamonds, 2);
        Card king = new Card(Card.Color.Diamonds, 13);
        humanPlayer.addCard(two);
        humanPlayer.addCard(three);
        humanPlayer.addCard(king);
        assertEquals(15, humanPlayer.getValue());
        assertFalse(humanPlayer.hasBlackJack());
    }

    @Test
    public void bothGotEvenValues() {


        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 13));
        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 9));

        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 13));
        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 9));

        testGame.printGameState();

        assertEquals(GameResult.Draw, testGame.evaluateCards());
    }

    @Test
    public void bothHaveBlackJack() {

        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 13));
        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 10));

        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 13));
        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 10));

        testGame.printGameState();

        assertEquals(GameResult.Draw, testGame.evaluateCards());
    }

    @Test
    public void dealerBlackJack() {

        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 13));
        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 10));

        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 5));
        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 10));

        testGame.printGameState();

        assertEquals(GameResult.DealerWins, testGame.evaluateCards());
    }

    @Test
    public void HuumanBlackJack() {

        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 10));
        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 10));

        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 13));
        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 1));

        testGame.printGameState();

        assertEquals(GameResult.PlayerWins, testGame.evaluateCards());
    }

    @Test
    public void dealerOut() {

        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 10));
        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 11));
        testGame.getDealer().addCard(new Card(Card.Color.Diamonds, 12));

        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 8));
        testGame.getHumanPlayer().addCard(new Card(Card.Color.Spades, 1));

        testGame.printGameState();

        assertEquals(GameResult.PlayerWins, testGame.evaluateCards());
    }


}
